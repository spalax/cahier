cahier 📓 One-directory-a-day calendar management
================================================

    I am no longer using this software, so it will not be improved. Feel free to
    ask questions and to submit bugs anyway, but this will not be my priority.

    -- Louis

What's new?
-----------

See `changelog <https://git.framasoft.org/spalax/cahier/blob/main/CHANGELOG.md>`_.

Download and install
--------------------

See the end of list for a (quick and dirty) Debian package.

* From sources:

  * Non-Python dependency:
    Although this program can be used without them, it has be built to be used
    with `git <http://git-scm.com/>`_ and `ikiwiki <http://ikiwiki.info>`_.
  * Download: https://pypi.python.org/pypi/cahier
  * Install (in a `virtualenv`, if you do not want to mess with your distribution installation system)::

        python3 setup.py install

* From pip::

    pip install cahier

* Quick and dirty Debian (and Ubuntu?) package

  This requires `stdeb <https://github.com/astraw/stdeb>`_ to be installed::

      python3 setup.py --command-packages=stdeb.command bdist_deb
      sudo dpkg -i deb_dist/cahier-<VERSION>_all.deb

Documentation
-------------

* The compiled documentation is available on `readthedocs <http://cahier.readthedocs.io>`_

* To compile it from source, download and run::

      cd doc && make html
