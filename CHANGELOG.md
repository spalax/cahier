* cahier 0.1.2 (unreleased)

    * Add python3.6 support.

    -- Louis Paternault <spalax@gresille.org>

* cahier 0.1.1 (2015-06-13)

    * Python3.5 support
    * Several minor improvements to setup, test and documentation.

    -- Louis Paternault <spalax@gresille.org>

* cahier 0.1.0 (2015-03-20)

    * First published version.

    -- Louis Paternault <spalax@gresille.org>
